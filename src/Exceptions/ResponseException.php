<?php

namespace Ossycodes\Ugm\Exceptions;

class ResponseException extends \Exception
{
    public const JSON_INVALID = -1;
}

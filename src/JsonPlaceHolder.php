<?php

namespace Ossycodes\Ugm;

use Ossycodes\Ugm\Models\Response;
use Ossycodes\Ugm\JsonPlaceHolderClient;

class JsonPlaceHolder
{
    protected $api;

    protected $handler;

    /**
     * gets a post https://jsonplaceholder.typicode.com/posts/1
     * 
     * @param int $id 
     */
    public function getPost($id = 1)
    {
        $response = $this->api()->get("posts/${id}");

        if (!$response->isSuccess()) {
            return $response;
        }

        $body = Response::forResponse($response)->initialize((array) $response->getResult());

        return $body;
    }

    /**
     * get all posts https://jsonplaceholder.typicode.com/posts
     */
    public function getPosts()
    {
        $response = $this->api()->get('/posts');

        if (!$response->isSuccess()) {
            return $response;
        }

        $body = Response::forResponse($response)->initialize((array) $response->getResult());

        return $body;
    }

    /**
     * creates a post https://jsonplaceholder.typicode.com/posts
     * @param array $data
     */
    public function createPost(array $data)
    {
        $response = $this->api()->post('/posts', $data);

        if (!$response->isSuccess()) {
            return $response;
        }

        $body = Response::forResponse($response)->initialize((array) $response->getResult());

        return $body;
    }

    /**
     * initailizes the http client
     */
    public function api()
    {
        if ($this->api) {
            return $this->api;
        }

        $this->api = new JsonPlaceHolderClient();

        $this->api
            ->initClient($this->handler);

        return $this->api;
    }

    /**
     * set custom handler for unit tests
     * 
     * @param \GuzzleHttp\Handler\MockHandler|null $handler
     * @return mixed
     */
    public function setClientHandler($handler)
    {
        $this->handler = $handler;

        return $this;
    }
}

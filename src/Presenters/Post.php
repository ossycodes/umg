<?php

namespace Ossycodes\Ugm\Presenters;

use Ossycodes\Ugm\Base\RequestPresenter;

class Post extends RequestPresenter
{
    /**
     * @param array $info
     * @return Symfony\Component\HttpFoundation\ParameterBag
     */
    public function setPostInfo($info)
    {
        return $this->setTitle($info['title'])
            ->setBody($info['body'])
            ->setUserId($info['userId']);
    }
}

<?php

namespace Ossycodes\Ugm;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Ossycodes\Ugm\Skeleton\SkeletonClass
 */
class UgmFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ugm';
    }
}

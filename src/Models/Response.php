<?php

namespace Ossycodes\Ugm\Models;

use Ossycodes\Ugm\Base\ResponseModel;

/**
 * Class Response.
 *
 *
 * @method string getResult()
 * @method int getCode()
 * @method string getMessage()
 * @method string getRedirectUrl()
 * @method array getValidationErrors()
 * @method int getId()
 */
class Response extends ResponseModel
{
}

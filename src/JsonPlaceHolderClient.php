<?php

namespace Ossycodes\Ugm;

use GuzzleHttp\Client;
use Ossycodes\Ugm\Contracts\ApiClient;
use Psr\Http\Message\ResponseInterface;
use Ossycodes\Ugm\Exceptions\ResponseException;

class JsonPlaceHolderClient extends ApiClient
{
    protected $BASE_URI = 'https://jsonplaceholder.typicode.com';

    protected $client;

    /**
     * @param \GuzzleHttp\HandlerStack|null $handler 
     */
    public function initClient($handler = null)
    {
        $config = [
            'base_uri' => $this->BASE_URI,
            'Accept'      => 'application/json',
            'http_errors' => false,
            'connect_timeout'   => 30,
            'timeout'           => 30,
            'read_timeout'      => 30,
        ];

        //allows us to basically pass in a mock handler for our tests
        if ($handler) {
            $config['handler'] = $handler;
        }

        return $this->client = new Client($config);
    }

    protected function checkError(ResponseInterface $response): void
    {
        $json = $this->formatted($response);
     
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new ResponseException(sprintf('Json response not valid, status code: %s', $response->getStatusCode()), ResponseException::JSON_INVALID);
        }

        // if (in_array($response->getStatusCode(), [200, 201], true)) {
        //     return;
        // }

        // if (($json->result ?? 'failed') === 'failed') {
        //     throw new ResponseException($json->message . ' ' . implode(',', (array) ($json->validation_errors ?? [])), $response->getStatusCode());
        // }
    }

    public function formatted(ResponseInterface $response)
    {
        return json_decode(str_replace(["\r\n", "\n", "\r"], '', trim((string) $response->getBody())));
    }
}

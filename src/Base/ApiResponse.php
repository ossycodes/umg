<?php

namespace Ossycodes\Ugm\Base;

class ApiResponse
{
    protected $response = [];

    protected $message = null;

    protected $code = null;

    protected $success = false;

    private function __construct($response = null)
    {
        $this->response = $response;
    }

    /**
     * @param \Closure $closure
     *
     * @return ApiResponse
     */
    public static function fromClosure(\Closure $closure): self
    {
        try {
            return self::fromResponse($closure());
        } catch (\Exception $exception) {
            return self::fromException($exception);
        }
    }

    /**
     * @param \stdClass|null|string $response
     *
     * @return ApiResponse
     */
    public static function fromResponse($response): self
    {
        return (new static($response))->setSuccess(true);
    }

    /**
     * @param \Throwable $exception
     *
     * @return ApiResponse
     */
    public static function fromException($exception): self
    {
        return tap(new static(), function (ApiResponse $response) use ($exception) {
            $response->setMessage($exception->getMessage() ?: 'Unknown')
                ->setCode($exception->getCode());
        });
    }

    /**
     * @param int $code
     *
     * @return ApiResponse
     */
    public function setCode($code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return ApiResponse
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Determine if request is success.
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     *
     * @return ApiResponse
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get result from response.
     *
     * @return array|string
     */
    public function getResult()
    {
        return $this->response;
    }

    /**
     * Get first error.
     *
     * @return string
     */
    public function getErrorMessage(): ?string
    {
        return $this->message;
    }

    /**
     * Get first error code.
     *
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->code;
    }
}

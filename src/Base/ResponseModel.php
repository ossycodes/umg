<?php

namespace Ossycodes\Ugm\Base;

use Ossycodes\Ugm\Traits\HasParameters;

// use Salla\ApiResponse\ApiResponse;
// use Salla\ApiResponse\Contracts\ResponseModel as ResponseModelInterface;

abstract class ResponseModel extends ApiResponse 
{
    use HasParameters {
        __construct as __constructParameters;
    }

    /**
     * RequestModel constructor.
     *
     * @param null $response
     */
    public function __construct($response = null)
    {
        $this->__constructParameters();
        $this->response = $response;
    }

    /**
     * @param \Salla\ApiResponse\ApiResponse $apiResponse
     * @return \Salla\ApiResponse\Base\ResponseModel|ApiResponse|static
     */
    public static function forResponse(ApiResponse $apiResponse)
    {
        return (new static($apiResponse->getResult()))->setSuccess($apiResponse->isSuccess());
    }
}
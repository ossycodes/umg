<?php

namespace Ossycodes\Ugm\Base;

use Ossycodes\Ugm\Traits\HasParameters;

abstract class RequestPresenter
{
    use HasParameters;
}
<?php

namespace Ossycodes\Ugm\Contracts;

use Ossycodes\Ugm\Base\ApiResponse;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface ApiInterface.
 *
 * @mixin ApiClient
 */
interface ApiInterface
{
    public function request(string $method, string $uri, array $data = [], array $headers = []): ApiResponse;

    public function post(string $uri, array $data = [], array $headers = []): ApiResponse;

    public function put(string $uri, array $data = [], array $headers = []): ApiResponse;

    public function patch(string $uri, array $data = [], array $headers = []): ApiResponse;

    public function delete(string $uri, array $data = [], array $headers = []): ApiResponse;

    public function formatted(ResponseInterface $response);
}

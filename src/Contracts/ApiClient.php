<?php

namespace Ossycodes\Ugm\Contracts;

use Ossycodes\Ugm\Base\ApiResponse;
use Psr\Http\Message\ResponseInterface;

abstract class ApiClient implements ApiInterface
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    protected $requestBodyType = null;

    public function get(string $uri, array $data = [], array $headers = []): ApiResponse
    {
        return $this->request('get', $uri, $data, $headers);
    }

    public function request(string $method, string $uri, array $data = [], array $headers = []): ApiResponse
    {
        return ApiResponse::fromClosure(function () use ($method, $uri, $data, $headers) {
            if (! in_array($method, ['get', 'post', 'put', 'patch', 'delete'])) {
                throw new \InvalidArgumentException('Request method must be get, post, put, patch, or delete');
            }

            /** @var ResponseInterface $response */
            $response = $this->client->$method($uri, [
                'headers'                                                 => $headers,
                $method === 'get' ? 'query' : $this->getRequestBodyType() => $data,
            ]);

            $this->checkError($response);
    
            return $this->formatted($response);
        });
    }

    /**
     * @return null
     */
    public function getRequestBodyType()
    {
        return $this->requestBodyType ?: 'json';
    }

    /**
     * @param null $requestBodyType
     */
    public function setRequestBodyType($requestBodyType): void
    {
        $this->requestBodyType = $requestBodyType;
    }

    abstract protected function checkError(ResponseInterface $response);

    public function formatted(ResponseInterface $response)
    {
        return json_decode($response->getBody());
    }

    public function post(string $uri, array $data = [], array $headers = []): ApiResponse
    {
        return $this->request('post', $uri, $data, $headers);
    }

    public function put(string $uri, array $data = [], array $headers = []): ApiResponse
    {
        return $this->request('put', $uri, $data, $headers);
    }

    public function patch(string $uri, array $data = [], array $headers = []): ApiResponse
    {
        return $this->request('patch', $uri, $data, $headers);
    }

    public function delete(string $uri, array $data = [], array $headers = []): ApiResponse
    {
        return $this->request('delete', $uri, $data, $headers);
    }

    // protected function getHandler()
    // {
    //     if (app('env') === 'testing' && app()->has('MockHandler')) {
    //         return app('MockHandler');
    //     }
    // }
}

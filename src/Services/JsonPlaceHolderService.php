<?php

namespace Ossycodes\Ugm\Services;

use Ossycodes\Ugm\JsonPlaceHolder;
use Ossycodes\Ugm\Presenters\Post;

class JsonPlaceHolderService
{
    public function getPost()
    {
        $post = (new JsonPlaceHolder())->getPost();

        if ($post->isSuccess()) {
            return $post;
        }

        return $post;
    }

    public function getPosts()
    {
        $posts = (new JsonPlaceHolder())->getPosts();

        if ($posts->isSuccess()) {
            return $posts;
        }

        return $posts;
    }

    public function createPost(array $payload)
    {
        $postInfo = (new Post())->setPostInfo($payload)->toArray();
        $createdPost = (new JsonPlaceHolder())->createPost($postInfo);

        if ($createdPost->isSuccess()) {
            return $createdPost;
        }
        return $createdPost;
    }
}

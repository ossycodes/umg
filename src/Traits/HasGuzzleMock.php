<?php

namespace Ossycodes\Ugm\Traits;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Handler\MockHandler;

trait HasGuzzleMock
{
    /**
     * The being mock handler instance.
     *
     * @var MockHandler
     */
    protected $mockHandler;

    /**
     * the path to the mocked responses
     */
    protected $mockPath;

    final public function getMockHandler()
    {
        if ($this->mockHandler === null) {
            $this->mockHandler = new MockHandler();
        }

        return $this->mockHandler;
    }

    final protected function getHandlerStack(): HandlerStack
    {
        return HandlerStack::create($this->getMockHandler());
    }

    /**
     * mock http client response with given file
     * 
     * @param string $file
     * @return void
     */
    public function mockRequest($file)
    {
        $mockedResponse = $this->loadMockResponse($file);

        $parsedResponse = Message::parseResponse($mockedResponse);

        $this->mockHandler->append($parsedResponse);
    }

    /**
     * loads the mock response file
     * 
     * @param string $file
     * @return string
     */
    final public function loadMockResponse($file)
    {
        return file_get_contents($this->mockPath . '/' . ltrim($file, '/'));
    }

    public function setMockPath($path)
    {
        $this->mockPath = $path;

        return $this;
    }
}

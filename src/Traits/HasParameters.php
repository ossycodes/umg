<?php

namespace Ossycodes\Ugm\Traits;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\ParameterBag;

trait HasParameters
{
    /**
     * The card parameters.
     *
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    protected $parameters;

    final public function __construct(array $parameters = [])
    {
        $this->initialize($parameters);
    }

    /**
     * Initialize the object with parameters.
     *
     * If any unknown parameters passed, they will be ignored.
     *
     * @param array $parameters An associative array of parameters
     *
     * @return $this
     */
    public function initialize(array $parameters = null): self
    {
        $this->parameters = new ParameterBag($parameters);

        return $this;
    }

    /**
     * Support magic functions.
     *
     * ```
     * $this->setUsername('test'); // add a new element call username to parameters bag.
     * $this->getUsername(); // get a value of username element from parameters bag.
     * ```
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (Str::startsWith($name, 'set')) {
            return $this->setParameter(Str::after(Str::snake($name), 'set_'), ...$arguments);
        }

        if (Str::startsWith($name, 'get')) {
            return $this->getParameter(Str::after(Str::snake($name), 'get_'));
        }
    }

    /**
     * Set one parameter.
     *
     * @param string $key   Parameter key
     * @param mixed  $value Parameter value
     *
     * @return $this
     */
    protected function setParameter($key, $value)
    {
        $this->parameters->set($key, $value);

        return $this;
    }

    /**
     * Get one parameter.
     *
     * @return mixed A single parameter value.
     */
    protected function getParameter($key)
    {
        return $this->parameters->get($key);
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->getArrayMap($this->getParameters());
    }

    /**
     * automatic convert all inside object.
     *
     * @param $data
     * @return array
     */
    protected function getArrayMap($data)
    {
        if (is_array($data)) {
            return array_map(function ($data) {
                return $this->getArrayMap($data);
            }, $data);
        }

        return \is_object($data) ? $data->toArray() : $data;
    }

    /**
     * Get all parameters.
     *
     * @return array An associative array of parameters.
     */
    public function getParameters(): array
    {
        return $this->parameters->all();
    }
}

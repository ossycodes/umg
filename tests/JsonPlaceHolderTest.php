<?php

namespace Ossycodes\Ugm\Tests;

use GuzzleHttp\Psr7\Message;
use Ossycodes\Ugm\Tests\TestCase;
use Ossycodes\Ugm\JsonPlaceHolder;
use Ossycodes\Ugm\Presenters\Post;
use Ossycodes\Ugm\Traits\HasGuzzleMock;

class JsonPlaceHolderTest extends TestCase
{
  use HasGuzzleMock;

  public function setUp(): void
  {
    $this->api = new JsonPlaceHolder();

    $this->api->setClientHandler($this->getHandlerStack());

    $this->setMockPath(__DIR__ . '/Moc');
  }

  /**
   * @test
   */
  function it_can_get_all_posts()
  {
    $this->mockRequest('posts-parsed.txt');
    $expected = file_get_contents(__DIR__ . '/Moc/posts.txt');

    $result = $this->api->getPosts();

    $this->assertEquals($expected, $result);
  }

  /**
   * @test
   */
  public function it_can_get_a_single_post()
  {
    $this->mockRequest('single-post-parsed.txt');
    $expected = file_get_contents(__DIR__ . '/Moc/single-post.txt');

    $result = $this->api->getPost(1);

    // if (!$result->isSuccess()) {
    //   $result->getErrorMessage();
    // }

    // throw new \InvalidArgumentException(sprintf('Failed to create an order at daftra: %s', $result->getErrorMessage()));

    $this->assertEquals($expected, $result);
  }

  /**
   * @test
   */
  public function it_can_create_a_post()
  {
    $this->mockRequest('create-post-parsed.txt');
    $expected = file_get_contents(__DIR__ . '/Moc/create-post.txt');

    $postInfo =  [
      'title' => 'foo',
      'body' => 'bar',
      'userId' => 1,
    ];

    //use the request presenter to create the post
    // $postInfo = (new Post())->setPostInfo($postInfo);
    // dump($postInfo->getTitle());
    // dump($postInfo->getBody());
    // dd($postInfo->getUserId());

    $postInfo = (new Post())->setPostInfo($postInfo)->toArray();
    $result = $this->api->createPost($postInfo);

    $this->assertEquals($expected, $result);
  }

  /**
   * @test
   */
  public function testing_how_mocking_works()
  {
    $body = file_get_contents(__DIR__ . '/Moc/single-post-parsed.txt');
    $expected = file_get_contents(__DIR__ . '/Moc/single-post.txt');

    /**
     * $mock = new MockHandler([new Response($status, [], $body)]);
     * $handler = HandlerStack::create($mock);
     * you can pass the mock response directly to the Mockhandler's constructor
     * the problem with this is that: you will have to do always do this,
     * which is not flexible enough tbh.
     * A better approach will be using the `append()` method to dynamically
     * pass your mocked response to the MockHandler.
     */
    $mock = new \GuzzleHttp\Handler\MockHandler();
    $handler = \GuzzleHttp\HandlerStack::create($mock);

    $mock->append(Message::parseResponse($body));

    $client = (new JsonPlaceHolder())->setClientHandler($handler);

    $result = $client->getPost(1);

    $this->assertEquals($expected, $result);
  }

  /**
   * @test
   */
  public function sample_usage()
  {
    // $client = new \Ossycodes\Ugm\JsonPlaceHolderClient();

    // $client->initClient();

    // $response = $client->get('posts/1');

    // $response = \Ossycodes\Ugm\Models\Response::forResponse($response)->initialize((array) $response->getResult());

    // dump($response->getUserId());
    // dump($response->getTitle());
    // dump($response->getBody());
    // dd($response);

    /**
     * usage with Service
     */

    //get post
    // $response =((new \Ossycodes\Ugm\Services\JsonPlaceHolderService)->getPost());
    // if($response->isSuccess()) {
    //   dump($response->getUserId());
    //   dump($response->getTitle());
    //   dump($response->getBody());
    //   dd($response);
    // }

    //create post
    // $response =((new \Ossycodes\Ugm\Services\JsonPlaceHolderService)->createPost(
    //   $payload =  [
    //     'title' => 'foo',
    //     'body' => 'bar',
    //     'userId' => 1,
    //   ]
    // ));

    // if($response->isSuccess()) {
    //   dump($response->getId());
    //   dump($response->getUserId());
    //   dump($response->getTitle());
    //   dump($response->getBody());
    //   dd($response);
    // }

    //get posts
    // $response = ((new \Ossycodes\Ugm\Services\JsonPlaceHolderService)->getPosts());

    // foreach ($response->getParameters() as $res) {
    //   dump($res->id);
    //   dump($res->userId);
    //   dump($res->title);
    //   dump($res->body);
    // }

    //dd($response);

    $this->assertTrue(true);
  }
}

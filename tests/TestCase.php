<?php

namespace Ossycodes\Ugm\Tests;

use Ossycodes\Ugm\UgmServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [UgmServiceProvider::class];
    }

    protected function getEnvironmentSetUp($app)
    {
      // perform environment setup
    }
}

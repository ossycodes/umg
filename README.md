# Mocking guzzle and testing external API with PHPUnit (unit tests)

This is me trying to fully understand the "Mocking guzzle and testing external APIs with PHPUnit concept", which I now completely understand :-)

So what I did was create a "thin wrapper/Client Lib/SDK" for [jsonplaceholder](https://jsonplaceholder.typicode.com/guide/), and then write tests which mocks the jsonplaceholder APIs (**please check the JsonPlaceHolderTest file**).

## new to mocking or need a refresher on how mocking with guzzle works ?

- [read this blog post ](http://www.inanzzz.com/index.php/post/7cwp/mocking-guzzle-and-testing-external-api-with-phpunit)

## Mocking 

When testing HTTP clients, you often need to simulate specific scenarios like returning a successful response, returning an error, or returning specific responses in a certain order. Because unit tests need to be predictable, easy to bootstrap, and fast, hitting an actual remote API is a test smell.

Guzzle provides a mock handler that can be used to fulfill HTTP requests with a response

[read more](https://docs.guzzlephp.org/en/stable/testing.html)

## Installation

You can install the package via composer:

```bash
composer require ossycodes/ugm
```

## Usage

``` php
<?php

use Ossycodes\Ugm\JsonPlaceHolder;

$jsonPlaceHolder = new JsonPlaceHolder();

// gets all posts (https://jsonplaceholder.typicode.com/posts)
$jsonPlaceHolder->getPosts();

// gets a single post (https://jsonplaceholder.typicode.com/posts/1)
$jsonPlaceHolder->getPost($postId = 1);

// creates a post (https://jsonplaceholder.typicode.com/posts)
$payload = [ 
    'title' => 'foo',
    'body' => 'bar',
    'userId' => 1,
    ];
$jsonPlaceHolder->createPost($playload);

```

```php
<?php

    //get post
    $response =((new \Ossycodes\Ugm\Services\JsonPlaceHolderService)->getPost());
    if($response->isSuccess()) {
        dump($response->getUserId());
        dump($response->getTitle());
        dump($response->getBody());
        dd($response);
    }

    //create post
    $response =((new \Ossycodes\Ugm\Services\JsonPlaceHolderService)->createPost(
        $payload =  [
            'title' => 'foo',
            'body' => 'bar',
            'userId' => 1,
        ]
    ));

    if($response->isSuccess()) {
        dump($response->getId());
        dump($response->getUserId());
        dump($response->getTitle());
        dump($response->getBody());
        dd($response);
    }

    //get posts
    $response = ((new \Ossycodes\Ugm\Services\JsonPlaceHolderService)->getPosts());

    foreach ($response->getParameters() as $res) {
        dump($res->id);
        dump($res->userId);
        dump($res->title);
        dump($res->body);
    }

    dd($response);

```

### Testing

``` bash
composer test
```

## Credits

- [Emmanuel Osaigbovo](https://github.com/ossycodes)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
